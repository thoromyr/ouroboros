#!/usr/bin/python

# combat simulator

import argparse
import sys
from signal import signal, SIGPIPE, SIG_DFL
import random
import os

debug   = False
verbose = 0

players = []
npcs    = []

class Weapon(object):
    """Ways in which a combant can be armed"""
    def __init__(self, name, canAttack, canDefend, damage=1):
        self.name   = name
        self.attack = canAttack
        self.defend = canDefend
        self.damage = damage
        self.isReady = True
        self.defendingBy = 'none'
        self.limAttacks = 1
        self.unbalanced = False
    def canAttack(self):
        return self.attack and self.isReady
    def canDefend(self):
        return self.defend and self.isReady
    def defenseTied(self):
        self.isReady = False
    def recover(self):
        self.isReady = True
    def madeAttack(self):
        if self.unbalanced:
            self.isReady = False
    def madeDefense(self):
        pass
    def newRound(self):
        pass
    def repair(self):
        pass
    def resetStatus(self):
        self.repair()
        self.isReady = True

class NoWeapon(Weapon):
    def __init__(self):
        super(NoWeapon, self).__init__('noweapon', False, False)
    def defenseTied(self):
        raise AttributeError("NoWeapon() cannot be used to defend but defenseTied() called anyway")
    def recover(self):
        raise AttributeError("NoWeapon() cannot become unready but recover() called anyway")

class SimpleWeapon(Weapon):
    def __init__(self):
        super(SimpleWeapon, self).__init__('simple', True, True)
        self.defendingBy = 'parried'

class Unarmed(Weapon):
    def __init__(self):
        super(Unarmed, self).__init__('unarmed', True, False)
    def defenseTied(self):
        raise AttributeError("Unarmed() cannot be used to defend but defenseTied() called anyway")

class TwoHanded(Weapon):
    """ TwoHanded Weapon
        A two-handed weapon can be used to both attack and parry.
        Although only one attack can be made any number of parries can be attempted although a 'parry' result will make the weapon unready
    """
    def __init__(self):
        super(TwoHanded, self).__init__('twohanded', True, True)
        self.defendingBy = 'parried'

class OneHanded(Weapon):
    """ OneHanded Weapon
        A one-handed weapon can be used to either attack /or/ parry, but not both
        If it is used to attempt an attack then it cannot be used to parry, regardless of whether or not the attack succeeded
        If it is used to parry (in a tie) then it cannot be used to attack or parry again in that round.

        It was parry just once, but because it only becomes an issue in the event of ties it makes almost no difference
        under most circumstances. And, as tracking the "oh no, you already declared a 'parry' defense' this round" is
        eliminated.
    """
    def __init__(self, name='onehanded'):
        super(OneHanded, self).__init__(name, True, True)
        self.limitOne = True
        self.defendingBy = 'parried'
    def canAttack(self):
        return super(OneHanded, self).canAttack() and self.limitOne
    def canDefend(self):
        return super(OneHanded, self).canDefend() and self.limitOne
    def madeAttack(self):
        super(OneHanded, self).madeAttack()
        self.limitOne = False
    def newRound(self):
        self.limitOne = True
    def resetStatus(self):
        super(OneHanded, self).resetStatus()
        self.limitOne = True

class Shield(Weapon):
    """ Shield
        A shield can be used to make any number of defenses until it runs out of 'points'
        Each tied defense reduces the points by one, once it reaches zero the shield can no longer be used to make defenses
        (That is, a tied result would injure the character)

        Although not normally allowed to make attacks, if this is overridden (e.g., by setting attack = True)
        then making any attack with the shield makes it unready preventing its use for either offense or defense
        until it has been recovered.

        Although it might be tempting to use the .unbalanced flag to handle this, the current arrangement permits
        the possibility of a shield that is even more unbalanced (perhaps would become unready in addition to
        losing a 'point' when tying an attack).
    """
    def __init__(self, size):
        super(Shield, self).__init__('shield', False, True)
        self.size = int(size)
        if self.size < 0:
            raise ValueError("self.size attempted to set negative value (%s) for shield" % self.size)
        self.maxSize = self.size
        self.defendingBy = 'blocked'
    def canDefend(self):
        # if somehow an attack was made, e.g., by setting 'self.attack = True' would become unready and have to be recovered
        if super(OneHanded, self).canDefend():
            return self.size > 0
        return False
    def madeAttack(self):
        self.isReady = False
    def defenseTied(self):
        # not calling Weapon.defenseTied() as that would make the shield unready
        # the damage to the shield is done instead of becoming unready
        self.size -= 1
    def repair(self):
        self.size = self.maxSize

class DicePool:
    """Dice Pool"""
    def __init__(self, dice):
        self.dice = dice
    def roll(self):
        score = 0
        mishaps = 0
        fortune = 0
        for i in range(self.dice):
            r = random.randint(1,6)
            if r == 1:
                mishaps += 1
            elif r == 6:
                fortune += 1
        score = self.dice + fortune - mishaps
        return (score, fortune, mishaps)
    def quickRoll(self):
        (score, fortune, mishaps) = self.roll()
        return score
    def score(self):
        return self.quickRoll()

class Combatant(object):
    """ Combatant
        Represents a character involved in the fight. Has a stance (standing vs fallen), balance (vs off-balance),
        and can have equipment in the right and left hands.

        This class is responsible for managing weapons (equipped items) and how the character attacks and defends
        as well as recovering status, taking damage, and so on.
    """
    def __init__(self, args, side='none', count=0):
        # establish default values
        self.name = side + '_' + str(count)
        self.standing = True
        self.balanced = True
        self.rightHand = NoWeapon()
        self.leftHand = NoWeapon()
        self.attackingWith = '' # none, right, left
        self.defendingWith = '' # none, right, left
        self.defendingBy = '' # none, parry, block, dodge
        # args is a comma delimited string consisting of 
        #   [name],dice,[weapon,[armor|method]]
        terms = args.split(',')
        if len(terms) == 1:
            try:
                self.dice = int(terms[0])
                self.rightHand = SimpleWeapon()
            except ValueError:
                if verbose > 0:
                    print >> sys.stderr, "[EEE] single-value combatant '%s' expected integer" % terms[0]
            if self.dice < 1:
                raise ValueError("attempted to instantiate 'Combatant' class with dice pool of less than one (%s)" % self.dice)
        else:
            # is name given?
            try:
                int(terms[0])
            except ValueError:
                # first term must be the name...
                self.name = terms.pop(0)
            self.dice = int(terms.pop(0))
            if len(terms) > 0:
                weapon = terms.pop(0).lower()
                if weapon == 'simple':
                    self.rightHand = SimpleWeapon()
                elif weapon == 'unarmed':
                    self.rightHand = Unarmed()
                elif weapon == 'twohanded':
                    self.rightHand = TwoHanded()
                elif weapon == 'onehanded':
                    self.rightHand = OneHanded()
                elif weapon == 'twoweapon':
                    self.rightHand = OneHanded()
                    self.leftHand  = OneHanded()
                elif weapon == 'shield':
                    self.leftHand = Shield(self.dice)
                elif weapon == 'weaponshield':
                    self.rightHand = OneHanded()
                    self.leftHand  = Shield(self.dice)
                else:
                    print >> sys.stderr, "[EEE] Unhandled weapon type of (%s)" % weapon

                # additional terms?
                for term in terms:
                    # identify by keys
                    if term.lower() == 'heavyweapon':
                        # make damage 2
                        #   but 1-h becomes unready
                        #   and 2-h is limited
                        if weapon == 'twohanded':
                            self.rightHand = OneHanded('twohanded') # we use 1-h because operation is the same
                            self.rightHand.damage = 2
                        elif weapon == 'onehanded' or weapon == 'twoweapon':
                            self.rightHand.unbalanced = True
                            self.rightHand.damage = 2
                    elif term.lower() == 'twoattacks':
                        # allow two attacks
                        self.rightHand.limAttacks = 2
                        if weapon == 'onehanded' or weapon == 'twoweapon':
                            # one handed weapons become unready?
                            # just seems like there should be some sort of limiter
                            pass
                    elif term.lower() == 'threeattacks':
                        # allow three attacks (unarmed only?)
                        self.rightHand.limAttacks = 3
                    else:
                        print >> sys.stderr, "[WWW] Unhandled term", term
            else:
                self.rightHand = SimpleWeapon()
        self.hearts = self.dice
        self.dicepool = DicePool(self.dice)
        if debug and verbose > 3:
            print >> sys.stderr, "[III]", self.getStatus()
    def recover(self):
        if self.rightHand.isReady == False:
            self.rightHand.recover()
            return "readies " + self.rightHand.name
        elif self.leftHand.isReady == False:
            self.leftHand.recover()
            return "readies " + self.leftHand.name
        elif self.balanced == False: # ordering is important: must be balanced before can stand up
            self.balanced = True
            return "regains balance"
        elif self.standing == False:
            self.standing = True
            return "stands up"
        else:
            # should not be reachable...
            if debug:
                print >> sys.stderr, "[DDD] recover() called though nothing was unready"
    def canAttack(self):
        # note: cannot stand up when off balance so that is resolved
        if self.balanced == False or self.standing == False:
            # no attacking from the ground or when unbalanced
            self.attackingWith = 'none'
            return False
        elif self.rightHand.canAttack():
            self.attackingWith = 'right'
        elif self.leftHand.canAttack():
            self.attackingWith = 'left'
        else:
            self.attackingWith = 'none'
            return False
        return True
    def canDefend(self):
        if self.leftHand.canDefend():
            self.defendingWith = 'left'
            self.defendingBy = self.leftHand.defendingBy
        elif self.rightHand.name == 'twohanded' and self.rightHand.canDefend():
            self.defendingWith = 'right'
            self.defendingBy = self.rightHand.defendingBy
        elif self.balanced == True:
            self.defendingWith = 'body'
            self.defendingBy = 'dodged'
        elif self.rightHand.canDefend():
            self.defendingWith = 'right'
            self.defendingBy = self.rightHand.defendingBy
        else:
            self.defendingWith = 'none'
            self.defendingBy = 'none'
            return False
        return True
    def madeAttack(self):
        if self.attackingWith == 'right':
            self.rightHand.madeAttack()
        elif self.attackingWith == 'left':
            self.leftHand.madeAttack()
        else:
            # none... so how did we get here?
            raise AttributeError("madeAttack() called even though no attackingWith listed")
    def madeDefense(self):
        if self.canDefend():
            if self.defendingWith == 'right':
                self.rightHand.madeDefense()
            elif self.defendingWith == 'left':
                self.leftHand.madeDefense()
            elif self.defendingWith == 'body':
                pass
            else:
                # none... gets called whether or not a defense was made
                pass
        else:
            self.defendingWith = 'none'
            self.defendingBy = 'none'
        return True
    def defenseTied(self):
        if self.defendingWith == 'right':
            self.rightHand.defenseTied()
        elif self.defendingWith == 'left':
            self.leftHand.defenseTied()
        elif self.defendingWith == 'body':
            self.balanced = False
        else:
            # none... so how did we get here?
            raise AttributeError("defenseTied() called even though no defendingWith listed")
    def getDamage(self):
        if self.attackingWith == 'right':
            return self.rightHand.damage
        elif self.attackingWith == 'left':
            return self.leftHand.damage
        else:
            # none... so how did we get here?
            raise AttributeError("getDamage() called even though no attackingWith listed")
    def newRound(self):
        self.attackingWith = 'none'
        self.defendingWith = 'none'
        self.defendingBy = 'none'
        self.rightHand.newRound()
        self.leftHand.newRound()
    def repair(self):
        self.rightHand.repair()
        self.leftHand.repair()
    def heal(self, hearts=1):
        if self.dice > self.hearts:
            self.hearts += 1
    def healAll(self):
        self.hearts = self.dice
    def resetStatus(self):
        self.hearts = self.dice
        self.attackingWith = 'none'
        self.defendingWith = 'none'
        self.defendingBy = 'none'
        self.rightHand.resetStatus()
        self.leftHand.resetStatus()
    def getStatus(self):
        stance = 'fallen'
        if self.standing:
            stance = 'standing'
        balance = 'off-balance'
        if self.balanced:
            balance = 'balanced'
        try:
            if self.rightHand.isReady:
                rightReady = 'rRdy'
            else:
                rightReady = '!rRdy'
        except AttributeError:
            rightReady = 'n/a'
        try:
            if self.leftHand.isReady:
                leftReady = 'lRdy'
            else:
                leftReady = '!lRdy'
        except AttributeError:
            leftReady = 'n/a'
        return "******* %s: %dh %s %s %s %s %s %s %s" % (self.name, self.hearts, stance, balance, rightReady, leftReady, self.attackingWith, self.defendingWith, self.defendingBy)

class Player(Combatant):
    """Player character combatants"""
    count = 0
    def __init__(self, args, name='players'):
        Player.count += 1
        super(Player, self).__init__(args, name, Player.count)

class Nonplayer(Combatant):
    """NPC combatants"""
    count = 0
    def __init__(self, args, name='npcs'):
        Nonplayer.count += 1
        super(Nonplayer, self).__init__(args, name, Nonplayer.count)

def loadCombatants(filename, opts, side=''):
    global players, npcs
    with open(filename) as file:
        for line in file:
            line = line.rstrip()
            if (line[0] == 'a' or line[0] == 'b' or line[0] == 'p' or line[0] == 'n') and (line[1] == ',' or line[1] == ' '):
                if line[0] == 'a' or line[0] == 'p':
                    if side == opts.side_a or side == '':
                        players.append(Player(line[2:], opts.side_a))
                    else:
                        pass
                        # skipping due to filtering
                else:
                    if side == opts.side_b or side == '':
                        npcs.append(Nonplayer(line[2:], opts.side_b))
                    else:
                        pass
                        # skipping due to filtering
            else:
                # file does not specify, interpret according to side_a
                if opts.side_a == side:
                    players.append(Player(line, opts.side_a))
                else:
                    npcs.append(Nonplayer(line, opts.side_b))

def main(args):
    global verbose, debug, players, npcs
    parser = argparse.ArgumentParser(description='Ourobos combat simulator', epilog='weapon: simple, unarmed, twohanded, onehanded, shield, weaponshield\narmor: heavyweapon, twoattacks\nTo avoid unicode errors in the console: "export PYTHONIOENCODING=UTF-8"')
    # Combat options
    parser.add_argument('-a', '--side_a', default="players", help='Name for side A in the fight')
    parser.add_argument('-b', '--side_b', default="npcs", help='Name for side B in the fight')
    parser.add_argument('-p', '--player', action='append', default=[], help='Add player with name,dice,weapon,armor')
    parser.add_argument('-n', '--npc', action='append', default=[], help='Add npc with name,dice,weapon,armor')

    parser.add_argument('-c', '--cycles', type=int, default=1, help='Number of cycles to run')
    parser.add_argument('-s', '--stats', action='store_true', help='Print combat statistics')
    parser.add_argument('-f', '--file', action='append', default=[], help='File to read combatant attributes from')


    # Generic options
    parser.add_argument('-d', '--debug', action='store_true', help='Turn on debugging output [implies -vvv, use -q to suppress verbose output]')
    parser.add_argument('-v', '--verbose', action='count', default=0, help='Show non-fatal errors')
    parser.add_argument('-q', '--quiet', action='count', default=0, help='Suppress error messages')
    parser.add_argument('-V', '--version', action='version', version='%(prog)s 1.0 2019-04-29')

    opts = parser.parse_args(args)
    verbose = opts.verbose - opts.quiet
    if opts.debug:
        debug = opts.debug
        verbose += 3

    if verbose > 2:
        print >> sys.stderr, "[III] %s" % str(opts)[10:-1]

    # Initialize
    for player in opts.player:
        if os.path.isfile(player):
            loadCombatants(player, opts, opts.side_a)
        else:
            players.append(Player(player, opts.side_a))
    for npc in opts.npc:
        if os.path.isfile(npc):
            loadCombatants(npc, opts, opts.side_b)
        else:
            npcs.append(Nonplayer(npc, opts.side_b))
    for file in opts.file:
        loadCombatants(file, opts)

    if debug:
        for player in players:
            print player.name, player.dice
        for npc in npcs:
            print npc.name, npc.dice

    wins = {opts.side_a: 0, opts.side_b: 0}
    minTurns = {opts.side_a: 0, opts.side_b: 0}
    maxTurns = {opts.side_a: -1, opts.side_b: -1}
    totTurns = {opts.side_a: 0, opts.side_b: 0}

    # battle on!
    for i in range(opts.cycles):
        turn = 0
        for player in players:
            player.hearts = player.dice
            player.recover()
            player.repair()
            player.newRound()
        for npc in npcs:
            npc.hearts = npc.dice
            npc.recover()
            npc.repair()
            npc.newRound()

        while True:
            # new round...
            turn += 1
            for player in players:
                player.newRound()
            for npc in npcs:
                npc.newRound()

            # currently always give initiative to side_a (players)
            # first player attacks first npc, down the line
            if verbose > 0:
                print "turn:", turn
            idxNpc = 0
            for player in players:
                if player.hearts > 0 and player.canAttack():
                    # since the only valid attackingWith values are 'right' and 'left'
                    numAttacks = player.rightHand.limAttacks
                    if player.attackingWith == 'left':
                        numAttacks = player.leftHand.limAttacks
                    for attack in range(numAttacks):
                        if debug and verbose > 3:
                            print "--- %s can attack (%d hearts)" % (player.name, player.hearts)
                        for i in range(idxNpc, len(npcs)):
                            if npcs[i].hearts > 0:
                                # roll player attack
                                attack = player.dicepool.score()
                                player.madeAttack()
                                # roll npc defense
                                defense = npcs[i].dicepool.score()
                                npcs[i].madeDefense()
                                if debug or verbose > 1:
                                    print "\t%s (%d) vs %s (%d)" % (player.name, attack, npcs[i].name, defense)
                                elif verbose > 0:
                                    actionStr = "\t%s attacks %s" % (player.name, npcs[i].name)
                                if attack > defense:
                                    npcs[i].hearts -= player.getDamage()
                                    if debug or verbose > 1:
                                        print "\t* %s hits %s" % (player.name, npcs[i].name)
                                    elif verbose > 0:
                                        actionStr += " and hits"
                                elif attack == defense:
                                    if npcs[i].defendingWith != 'none':
                                        npcs[i].defenseTied()
                                        if debug or verbose > 1:
                                            print "\t- %s's attack %s by %s" % (player.name, npcs[i].defendingBy, npcs[i].name)
                                        elif verbose > 0:
                                            actionStr += " but is %s" % npcs[i].defendingBy
                                    else:
                                        if debug:
                                            print npcs[i].getStatus()
                                        npcs[i].hearts -= player.getDamage()
                                        if debug or verbose > 1:
                                            print "\t* %s hits %s (unable to defend)" % (player.name, npcs[i].name)
                                        elif verbose > 0:
                                            actionStr += " and hits (unable to defend)"
                                else:
                                    if not debug and verbose > 0:
                                        actionStr += " and misses"
                                if not debug and verbose == 1:
                                    print actionStr
                                if npcs[i].hearts < 1 and verbose > 0:
                                    print "\t*** %s is defeated" % npcs[i].name
                                break
                        if i < len(npcs):
                            idxNpc = i
                        else:
                            idxNpc = 0
                elif player.hearts > 0:
                    rec = player.recover()
                    if verbose > 0:
                        print "\t%s %s" % (player.name, rec)

            # check victory condition: no npc left with hearts > 0
            victory = True
            for npc in npcs:
                if npc.hearts > 0:
                    victory = False
                    break
            if victory:
                if verbose > -1:
                    print ">>> %s win after %d turns" % (opts.side_a, turn)
                wins[opts.side_a] += 1
                totTurns[opts.side_a] += turn
                if turn < minTurns[opts.side_a] or minTurns[opts.side_a] == 0:
                    minTurns[opts.side_a] = turn
                if turn > maxTurns[opts.side_a]:
                    maxTurns[opts.side_a] = turn
                break

            # first npc attacks first player, down the line and cycling as needed
            idxPlayer = 0
            for npc in npcs:
                if npc.hearts > 0 and npc.canAttack():
                    # since the only valid attackingWith values are 'right' and 'left'
                    numAttacks = npc.rightHand.limAttacks
                    if npc.attackingWith == 'left':
                        numAttacks = npc.leftHand.limAttacks
                    for attack in range(numAttacks):
                        if debug and verbose > 3:
                            print "--- %s can attack (%d hearts)" % (npc.name, npc.hearts)
                        for i in range(idxPlayer, len(players)):
                            if players[i].hearts > 0:
                                # roll npc attack
                                attack = npc.dicepool.score()
                                npc.madeAttack()
                                # roll npc defense
                                defense = players[i].dicepool.score()
                                players[i].madeDefense()
                                if debug or verbose > 1:
                                    print "\t%s (%d) vs %s (%d)" % (npc.name, attack, players[i].name, defense)
                                elif verbose > 0:
                                    actionStr = "\t%s attacks %s" % (npc.name, players[i].name)
                                if attack > defense:
                                    players[i].hearts -= npc.getDamage()
                                    if debug or verbose > 1:
                                        print "\t* %s hits %s" % (npc.name, players[i].name)
                                    elif verbose > 0:
                                        actionStr += " and hits"
                                elif attack == defense:
                                    if players[i].defendingWith != 'none':
                                        players[i].defenseTied()
                                        if debug or verbose > 1:
                                            print "\t- %s's attack %s by %s" % (npc.name, players[i].defendingBy, players[i].name)
                                            if debug:
                                                shields = 0
                                                if players[i].defendingWith == 'left':
                                                    name = players[i].leftHand.name
                                                    if players[i].leftHand.name == 'shield':
                                                        shields = players[i].leftHand.size

                                                print "[DDD]", players[i].defendingWith, players[i].defendingBy, name, shields
                                                print players[i].getStatus()
                                        elif verbose > 0:
                                            actionStr += " but is %s" % players[i].defendingBy
                                    else:
                                        if debug:
                                            print players[i].getStatus()
                                        players[i].hearts -= npc.getDamage()
                                        if debug or verbose > 1:
                                            print "\t* %s hits %s (unable to defend)" % (npc.name, players[i].name)
                                        elif verbose > 0:
                                            actionStr += " and hits (unable to defend)"
                                else:
                                    if not debug and verbose > 0:
                                        actionStr += " and misses"
                                if not debug and verbose == 1:
                                    print actionStr
                                if players[i].hearts < 1 and verbose > 0:
                                    print "\t*** %s is defeated" % players[i].name
                                break
                        if i < len(players):
                            idxPlayer = i
                        else:
                            idxPlayer = 0
                elif npc.hearts > 0:
                    rec = npc.recover()
                    if verbose > 0:
                        print "\t%s %s" % (npc.name, rec)

            # check victory condition: no player left with hearts > 0
            victory = True
            for player in players:
                if player.hearts > 0:
                    victory = False
                    break
            if victory:
                if verbose > -1:
                    print "<<< %s win after %d turns" % (opts.side_b, turn)
                wins[opts.side_b] += 1
                totTurns[opts.side_b] += turn
                if turn < minTurns[opts.side_b] or minTurns[opts.side_b] == 0:
                    minTurns[opts.side_b] = turn
                if turn > maxTurns[opts.side_b]:
                    maxTurns[opts.side_b] = turn
                break


    if opts.stats:
        print "-" * 25
        try:
            avg = totTurns[opts.side_a] * 1.0 / wins[opts.side_a]
        except ZeroDivisionError:
            avg = 0
        print "%s\t%d (%0.2f%%)\t%d/%d/%0.2f" % (opts.side_a, wins[opts.side_a], 100.0 * wins[opts.side_a] / opts.cycles, minTurns[opts.side_a], maxTurns[opts.side_a], avg)
        try:
            avg = totTurns[opts.side_b] * 1.0 / wins[opts.side_b]
        except ZeroDivisionError:
            avg = 0
        print "%s\t%d (%0.2f%%)\t%d/%d/%0.2f" % (opts.side_b, wins[opts.side_b], 100.0 * wins[opts.side_b] / opts.cycles, minTurns[opts.side_b], maxTurns[opts.side_b], avg)
        minT = minTurns[opts.side_a]
        if minT > minTurns[opts.side_b]:
            minT = minTurns[opts.side_b]
        maxT = maxTurns[opts.side_a]
        if maxT < maxTurns[opts.side_b]:
            maxT = maxTurns[opts.side_b]
        totT = totTurns[opts.side_a] + totTurns[opts.side_b]
        print "\t\t\t" + "-" * 10
        print "\t\t\t%d/%d/%0.2f" % (minT, maxT, totT * 1.0 / opts.cycles)
    else:
        print "-" * 25
        print "%s\t%d (%0.2f%%)" % (opts.side_a, wins[opts.side_a], 100.0 * wins[opts.side_a] / opts.cycles)
        print "%s\t%d (%0.2f%%)" % (opts.side_b, wins[opts.side_b], 100.0 * wins[opts.side_b] / opts.cycles)

if __name__ == '__main__':
    # Restore default handler for SIG_PIPE (http://coding.derkeiler.com/Archive/Python/comp.lang.python/2004-06/3823.html)
    # this avoids a useless IOError() when piping to another application and that application closes early by simply restoring
    # the default handler
    signal(SIGPIPE, SIG_DFL)
    sys.exit(main(sys.argv[1:]))
